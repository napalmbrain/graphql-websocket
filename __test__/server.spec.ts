import { WebSocket } from "ws";
import { getPort } from "get-port-please";
import { gql } from "@apollo/client/core";

import { Client } from "../src/client";
import { Server } from "../src/server";
import { sleep } from "../src/utils";
import { Payload } from "../src/common";

import { typeDefs, resolvers } from "./lib/schema";

const host = "127.0.0.1";

describe("server", () => {
  it("provides a context", async () => {
    const test = jest.fn(() => {});
    const port = await getPort({ random: true });
    const server = new Server({
      options: {
        host,
        port
      },
      typeDefs: gql`
        type Query {
          hello: String
        }
      `,
      resolvers: {
        Query: {
          hello(root: any, args: any, context: any, info: any) {
            context.test();
          }
        }
      },
      context: async (context) => {
        context.test = test;
      }
    });
    const client = new Client({
      uri: `ws://${host}:${port}`,
      implementation: WebSocket
    });
    await server.start();
    client.subscibe(
      {
        query: gql`
          {
            hello
          }
        `
      },
      {}
    );
    await sleep(100);
    expect(test).toBeCalled();
    await client.disconnect();
    await server.stop();
  });

  it("can ping", async () => {
    const port = await getPort({ random: true });
    const server = new Server({
      options: {
        host,
        port
      },
      typeDefs: gql`
        type Query {
          hello: String
        }
      `,
      resolvers: {
        Query: {
          hello() {
            return "hello";
          }
        }
      }
    });
    const client = new Client({
      uri: `ws://${host}:${port}`,
      implementation: WebSocket
    });
    await server.start();
    await client.connected;
    let payload: Payload;
    const date = new Date().toISOString();
    const response: any = await new Promise((resolve) => {
      server.ping(
        //@ts-ignore: client.cid IS a string.
        client.cid,
        { date },
        {
          next: (p: Payload) => (payload = p),
          complete: () => resolve(payload)
        }
      );
    });
    expect(date).toEqual(response.date);
    await client.disconnect();
    await server.stop();
  });

  it("can ping subscriptions", async () => {
    const port = await getPort({ random: true });
    const server = new Server({
      options: {
        host,
        port
      },
      timeout: 1000,
      keepalive: 500,
      typeDefs,
      resolvers
    });
    const client = new Client({
      uri: `ws://${host}:${port}`,
      implementation: WebSocket,
      timeout: 1000
    });
    await server.start();
    await client.connected;
    const operation = client.subscibe({
      query: gql`
        subscription {
          greeting
        }
      `
    });
    await sleep(2000);
    expect(
      server.connections.get(client.cid || "")?.operations.get(operation.id)
    ).toBeDefined();
    await client.disconnect();
    await server.stop();
  });

  it("can timeout subscriptions", async () => {
    const port = await getPort({ random: true });
    const server = new Server({
      options: {
        host,
        port
      },
      timeout: 1000,
      keepalive: 500,
      operation: {
        timeout: 1000,
        keepalive: 2000
      },
      typeDefs,
      resolvers
    });
    const client = new Client({
      uri: `ws://${host}:${port}`,
      implementation: WebSocket,
      timeout: 1000
    });
    await server.start();
    await client.connected;
    const operation = client.subscibe({
      query: gql`
        subscription {
          greeting
        }
      `
    });
    await sleep(2000);
    const connection = server.connections.get(client.cid || "");
    expect(connection).toBeDefined();
    expect(connection?.operations.get(operation.id)).not.toBeDefined();
    await client.disconnect();
    await server.stop();
  });
});
