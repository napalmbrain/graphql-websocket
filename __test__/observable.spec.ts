import exp from "constants";
import { Observable } from "../src/observable";

describe("observable", () => {
  it("can be created without observers", () => {
    const observable = new Observable();
    expect(observable.observers.length).toEqual(0);
  });

  it("can be created with observers", () => {
    const observable = new Observable([
      {
        next: () => {}
      },
      {
        next: () => {}
      }
    ]);
    expect(observable.observers.length).toEqual(2);
  });

  it("can add observers", () => {
    const observable = new Observable();
    observable.observe({});
    expect(observable.observers.length).toEqual(1);
  });

  it("can remove observers", () => {
    const observer = {};
    const observable = new Observable([observer]);
    observable.unobserve(observer);
    expect(observable.observers.length).toEqual(0);
  });

  it("can call next", () => {
    const next = jest.fn(() => {});
    const observable = new Observable([{}, { next }]);
    observable.next({});
    expect(next).toBeCalledTimes(1);
  });

  it("can call complete", () => {
    const complete = jest.fn(() => {});
    const observable = new Observable([{}, { complete }]);
    observable.complete();
    expect(complete).toBeCalledTimes(1);
  });

  it("can call error", () => {
    const error = jest.fn(() => {});
    const observable = new Observable([{}, { error }]);
    observable.error({});
    expect(error).toBeCalledTimes(1);
  });

  it("can call end", () => {
    const end = jest.fn(() => {});
    const observable = new Observable([{}, { end }]);
    observable.end();
    expect(end).toBeCalledTimes(1);
  });

  it("can remove observers on end", () => {
    const observable = new Observable([{}]);
    observable.end();
    expect(observable.observers.length).toEqual(0);
  });
});
