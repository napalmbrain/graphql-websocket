import { WebSocket } from "ws";
import { getPort } from "get-port-please";
import { gql } from "graphql-tag";
import { merge } from "lodash";

import { Server } from "../src/server";
import { Client } from "../src/client";
import { sleep } from "../src/utils";

import { typeDefs, resolvers } from "./lib/schema";

let host: string;
let port: number;
let server: Server;
let client: Client;

beforeEach(async () => {
  host = "127.0.0.1";
  port = await getPort({ random: true });
  server = new Server({
    typeDefs,
    resolvers,
    options: {
      host,
      port
    }
  });
  await server.start();
  client = new Client({
    uri: `ws://${host}:${port}`,
    implementation: WebSocket
  });
});

afterEach(async () => {
  await client.disconnect();
  await server.stop();
});

describe("client", () => {
  it("can get its sid", async () => {
    await client.connected;
    expect(client.sid).toBeDefined();
  });

  it("can get its cid", async () => {
    await client.connected;
    expect(client.cid).toBeDefined();
  });

  it("can ping", async () => {
    const date = new Date().toISOString();
    const result: any = await client.ping({ date });
    expect(result.date).toEqual(date);
  });

  it("can set meta data", async () => {
    const result: any = await client.meta({ token: "value" });
    expect(result.meta.token).toEqual("value");
  });

  it("can get meta data", async () => {
    await client.meta({ some: "value" });
    const result: any = await client.meta();
    expect(result.meta.some).toEqual("value");
  });

  it("can set meta data from the constructor as an object", async () => {
    const client = new Client({
      uri: `ws://${host}:${port}`,
      implementation: WebSocket,
      meta: {
        test: "ing"
      }
    });
    const data = await client.meta();
    expect(data.meta.test).toEqual("ing");
  });

  it("can set meta data from the constructor as a function", async () => {
    const client = new Client({
      uri: `ws://${host}:${port}`,
      implementation: WebSocket,
      meta: () => ({
        test: "ing"
      })
    });
    const data = await client.meta();
    expect(data.meta.test).toEqual("ing");
  });

  it("can set meta data from the constructor as an async function", async () => {
    const client = new Client({
      uri: `ws://${host}:${port}`,
      implementation: WebSocket,
      meta: async () => ({
        test: "ing"
      })
    });
    const data = await client.meta();
    expect(data.meta.test).toEqual("ing");
  });

  it("can query", async () => {
    const result: any = await new Promise((resolve) => {
      let response: any;
      client.subscibe(
        {
          query: gql`
            {
              alpha
            }
          `
        },
        {
          next: (payload) => (response = payload),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.data.alpha).toEqual("alpha");
  });

  it("can query with variables in the query string", async () => {
    const result: any = await new Promise((resolve) => {
      let response: any;
      client.subscibe(
        {
          query: gql`
            {
              echo(value: "hello")
            }
          `
        },
        {
          next: (payload) => (response = payload),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.data.echo).toEqual("hello");
  });

  it("can query with variables as an argument", async () => {
    const result: any = await new Promise((resolve) => {
      let response: any;
      client.subscibe(
        {
          query: gql`
            query echo($value: String) {
              echo(value: $value)
            }
          `,
          variables: {
            value: "hello"
          }
        },
        {
          next: (payload) => (response = payload),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.data.echo).toEqual("hello");
  });

  it("can batch query", async () => {
    const result: any = await new Promise((resolve) => {
      let response: any;
      client.subscibe(
        {
          query: gql`
            query alpha {
              alpha
            }
            query beta {
              beta
            }
          `
        },
        {
          next: (payload) => (response = payload),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.data.alpha).toEqual("alpha");
    expect(result.data.beta).toEqual("beta");
  });

  it("can subscribe to a generator", async () => {
    const result: any = await new Promise((resolve) => {
      let response: any;
      client.subscibe(
        {
          query: gql`
            subscription {
              hi
            }
          `
        },
        {
          next: (payload) => (response = payload),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.data.hi).toEqual("hi");
  });

  it("can subscribe to pubsub", async () => {
    const result: any = await new Promise(async (resolve) => {
      client.subscibe(
        {
          query: gql`
            subscription greeting {
              greeting
            }
          `
        },
        {
          next: (payload) => resolve(payload)
        }
      );
      await sleep(100);
      client.subscibe({
        query: gql`
          query greet {
            greet
          }
        `
      });
    });
    await sleep(100);
    expect(result.data.greeting).toEqual("hello");
  });

  it("can batch subscribe to a generator", async () => {
    const result: any = await new Promise((resolve) => {
      let response = {};
      client.subscibe(
        {
          query: gql`
            subscription hi {
              hi
            }
            subscription hello {
              hello
            }
          `
        },
        {
          next: (payload) => (response = merge(response, payload)),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.data.hi).toEqual("hi");
    expect(result.data.hello).toEqual("hello");
  });

  it("can batch subscribe to pubsub", async () => {
    const result: any = await new Promise(async (resolve) => {
      let count = 0;
      let response = {};
      client.subscibe(
        {
          query: gql`
            subscription greeting {
              greeting
            }
            subscription testing {
              testing
            }
          `
        },
        {
          next: (payload) => {
            count++;
            response = merge(response, payload);
            if (count == 2) {
              resolve(response);
            }
          }
        }
      );
      await sleep(1000);
      client.subscibe({
        query: gql`
          query greet {
            greet
          }
          query test {
            test
          }
        `
      });
    });
    await sleep(1000);
    expect(result.data.greeting).toEqual("hello");
    expect(result.data.testing).toEqual("test");
  });

  it("errors on query throw", async () => {
    const result: any = await new Promise((resolve) => {
      let response: any;
      client.subscibe(
        {
          query: gql`
            {
              error
            }
          `
        },
        {
          error: (error) => (response = error),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.errors.length).toEqual(1);
  });

  it("errors on subscription throw", async () => {
    const result: any = await new Promise((resolve) => {
      let response: any;
      client.subscibe(
        {
          query: gql`
            subscription {
              error
            }
          `
        },
        {
          error: (error) => (response = error),
          complete: () => resolve(response)
        }
      );
    });
    expect(result.errors.length).toEqual(1);
  });

  it("errors on malformed query", async () => {
    try {
      client.subscibe({
        query: "x { hello }"
      });
    } catch (error) {
      expect((error as Error).message).toContain("x");
      return;
    }
    expect(true).toEqual(false);
  });

  it("can freeze and unfreeze", async () => {
    const result: any = await new Promise(async (resolve) => {
      client.subscibe(
        {
          query: gql`
            subscription {
              greeting
            }
          `
        },
        {
          next: (payload) => resolve(payload)
        }
      );
      client.freeze();
      client.unfreeze();
      await sleep(100);
      client.subscibe({
        query: gql`
          {
            greet
          }
        `
      });
    });
    expect(result.data.greeting).toEqual("hello");
  });

  it("can spam freeze and unfreeze", async () => {
    const result: any = await new Promise(async (resolve) => {
      client.subscibe(
        {
          query: gql`
            subscription {
              greeting
            }
          `
        },
        {
          next: (payload) => resolve(payload)
        }
      );
      client.freeze();
      client.unfreeze();
      client.freeze();
      client.unfreeze();
      client.freeze();
      client.unfreeze();
      await sleep(100);
      client.subscibe({
        query: gql`
          {
            greet
          }
        `
      });
    });
    expect(result.data.greeting).toEqual("hello");
  });

  it("can freeze and unfreeze with multiple queries", async () => {
    const result: any = await new Promise(async (resolve) => {
      let count = 0;
      let response = {};
      client.subscibe(
        {
          query: gql`
            subscription {
              greeting
            }
          `
        },
        {
          next: (payload) => {
            count++;
            response = merge(response, payload);
            if (count === 2) {
              resolve(response);
            }
          }
        }
      );
      client.subscibe(
        {
          query: gql`
            subscription {
              testing
            }
          `
        },
        {
          next: (payload) => {
            count++;
            response = merge(response, payload);
            if (count === 2) {
              resolve(response);
            }
          }
        }
      );
      client.freeze();
      client.unfreeze();
      await sleep(100);
      client.subscibe({
        query: gql`
          query greet {
            greet
          }
          query test {
            test
          }
        `
      });
    });
    expect(result.data.greeting).toEqual("hello");
    expect(result.data.testing).toEqual("test");
  });

  it("can disconnect while frozen", async () => {
    const result: any = await new Promise(async (resolve) => {
      client.subscibe(
        {
          query: gql`
            subscription {
              greeting
            }
          `
        },
        {
          next: (payload) => resolve(payload)
        }
      );
      client.freeze();
      await client.disconnect();
      await client.connect();
      client.unfreeze();
      await sleep(100);
      client.subscibe({
        query: gql`
          {
            greet
          }
        `
      });
    });
    expect(result.data.greeting).toEqual("hello");
  });

  it("can complete a pending subscription", async () => {
    let called = 0;
    await new Promise<void>(async (resolve) => {
      const operation = client.subscibe(
        {
          query: gql`
            subscription {
              greeting
            }
          `
        },
        {
          complete: () => {
            called++;
            if (called == 2) {
              resolve();
            }
          }
        }
      );
      client.complete(operation, {
        complete: () => {
          called++;
          if (called == 2) {
            resolve();
          }
        }
      });
    });
    expect(called).toEqual(2);
  });

  it("can handle connection error", async () => {
    const error = jest.fn(() => {});
    let p: number;
    do {
      p = await getPort({ random: true });
    } while (port === p);
    const client = new Client({
      uri: `ws://${host}:${p}`,
      implementation: WebSocket,
      on: {
        error
      }
    });
    await sleep(1000);
    expect(error).toBeCalledTimes(1);
  });
});
