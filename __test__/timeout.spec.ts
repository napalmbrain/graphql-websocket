import { Timeout } from "../src/timeout";
import { sleep } from "../src/utils";

describe("timeout", () => {
  it("can be created with a callback", async () => {
    const callback = jest.fn(() => {});
    const timeout = new Timeout(callback, 50);
    await sleep(100);
    timeout.stop();
    expect(callback).toBeCalledTimes(1);
  });

  it("can be stopped", async () => {
    const callback = jest.fn(() => {});
    const timeout = new Timeout(callback, 50);
    timeout.stop();
    await sleep(100);
    expect(callback).not.toBeCalled();
  });

  it("can be started", async () => {
    const callback = jest.fn(() => {});
    const timeout = new Timeout(callback, 50);
    timeout.stop();
    timeout.start();
    await sleep(100);
    expect(callback).toBeCalledTimes(1);
  });

  it("can be promised", async () => {
    const timeout = new Timeout(() => {}, 50);
    const promise = timeout.promise();
    await promise;
    expect(true).toBe(true);
  });

  it("can be promised with a value", async () => {
    const timeout = new Timeout(() => {}, 50);
    const promise = timeout.promise({ test: "value" });
    const result = await promise;
    expect(result.test).toBe("value");
  });

  it("can be promised multiple times", async () => {
    const timeout = new Timeout(() => {}, 50);
    const alpha = timeout.promise("alpha");
    const beta = timeout.promise("beta");
    const gamma = timeout.promise("gamma");
    const result = await Promise.all([alpha, beta, gamma]);
    expect(result).toEqual(["alpha", "beta", "gamma"]);
  });
});
