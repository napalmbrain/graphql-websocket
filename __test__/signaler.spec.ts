import { Signaler } from "../src/signaler";

describe("signaler", () => {
  it("can be created with a callback", () => {
    const callback = jest.fn(() => {});
    const signaler = new Signaler(callback);
    signaler.execute();
    expect(callback).toBeCalledTimes(1);
  });

  it("can be promised", async () => {
    const signaler = new Signaler();
    const promise = signaler.promise();
    setTimeout(() => {
      signaler.execute();
    }, 50);
    await promise;
    expect(true).toBe(true);
  });

  it("can be promised with a value", async () => {
    const signaler = new Signaler();
    const promise = signaler.promise({ test: "value" });
    setTimeout(() => {
      signaler.execute();
    }, 50);
    const result = await promise;
    expect(result.test).toBe("value");
  });

  it("can be promised multiple times", async () => {
    const signaler = new Signaler();
    const alpha = signaler.promise("alpha");
    const beta = signaler.promise("beta");
    const gamma = signaler.promise("gamma");
    setTimeout(() => {
      signaler.execute();
    }, 50);
    const result = await Promise.all([alpha, beta, gamma]);
    expect(result).toEqual(["alpha", "beta", "gamma"]);
  });
});
