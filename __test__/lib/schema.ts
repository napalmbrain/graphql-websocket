import { GraphQLError } from "graphql";
import { RedisPubSub } from "graphql-redis-subscriptions";
import { gql } from "@apollo/client/core";

const pubsub = new RedisPubSub();

export const typeDefs = gql`
  type Query {
    alpha: String
    beta: String
    greet: String
    test: String
    error: String
    echo(value: String): String
  }

  type Subscription {
    hi: String
    hello: String
    greeting: String
    testing: String
    error: String
  }
`;

export const resolvers = {
  Query: {
    alpha() {
      return "alpha";
    },
    beta() {
      return "beta";
    },
    greet() {
      pubsub.publish("greeting", { greeting: "hello" });
    },
    test() {
      pubsub.publish("testing", { testing: "test" });
    },
    error() {
      throw new GraphQLError("This is an error.");
    },
    echo(parent: any, args: any) {
      return args.value;
    }
  },
  Subscription: {
    hi: {
      async *subscribe() {
        for (const hi of ["hi"]) {
          yield { hi };
        }
      }
    },
    hello: {
      async *subscribe() {
        for (const hello of ["hello"]) {
          yield { hello };
        }
      }
    },
    greeting: {
      subscribe() {
        return pubsub.asyncIterator("greeting");
      }
    },
    testing: {
      subscribe() {
        return pubsub.asyncIterator("testing");
      }
    },
    error: {
      subscribe() {
        throw new GraphQLError("This is an error.");
      }
    }
  }
};
