import { WebSocket } from "ws";
import { gql, ApolloClient, InMemoryCache } from "@apollo/client/core";
import { getPort } from "get-port-please";

import { Server } from "../src/server";
import { Link } from "../src/link";
import { sleep } from "../src/utils";

import { typeDefs, resolvers } from "./lib/schema";

let server: Server;
let client: ApolloClient<any>;
let link: Link;

beforeEach(async () => {
  const host = "127.0.0.1";
  const port = await getPort({ random: true });
  server = new Server({
    typeDefs,
    resolvers,
    options: {
      host,
      port
    }
  });
  link = new Link({
    uri: `ws://${host}:${port}`,
    implementation: WebSocket
  });
  client = new ApolloClient({
    cache: new InMemoryCache(),
    link
  });
  await server.start();
});

afterEach(async () => {
  await link.disconnect();
  await server.stop();
});

describe("link", () => {
  it("can query", async () => {
    const result = await client.query({
      query: gql`
        {
          alpha
        }
      `
    });
    expect(result.data.alpha).toEqual("alpha");
  });

  xit("can batch query", async () => {
    const result = await client.query({
      query: gql`
        query alpha {
          alpha
        }
        query beta {
          beta
        }
      `
    });
    console.log(result);
  });

  it("can subscribe to a generator", async () => {
    const result: any = await new Promise((resolve) => {
      const observer = client.subscribe({
        query: gql`
          subscription {
            hi
          }
        `
      });
      observer.subscribe({
        next: (payload) => resolve(payload)
      });
    });
    expect(result.data.hi).toEqual("hi");
  });

  it("can subscribe to pubsub", async () => {
    const result: any = await new Promise(async (resolve) => {
      const observer = client.subscribe({
        query: gql`
          subscription {
            greeting
          }
        `
      });
      observer.subscribe({
        next: (result) => resolve(result)
      });
      await sleep(100);
      await client.query({
        query: gql`
          {
            greet
          }
        `,
        fetchPolicy: "network-only"
      });
    });
    expect(result.data.greeting).toEqual("hello");
  });

  it("can freeze and unfreeze", async () => {
    const result: any = await new Promise(async (resolve) => {
      const observer = client.subscribe({
        query: gql`
          subscription {
            greeting
          }
        `
      });
      observer.subscribe({
        next: (result) => resolve(result)
      });
      await link.freeze();
      await link.unfreeze();
      await sleep(100);
      await client.query({
        query: gql`
          {
            greet
          }
        `,
        fetchPolicy: "network-only"
      });
    });
    expect(result.data.greeting).toEqual("hello");
  });

  it("can set meta data", async () => {
    const response = await link.meta({ test: "ing" });
    expect(response.meta.test).toEqual("ing");
  });

  it("can get meta data", async () => {
    await link.meta({ test: "ing" });
    const response = await link.meta();
    expect(response.meta.test).toEqual("ing");
  });
});
