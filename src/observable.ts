import remove from "lodash/remove";

import { Generic } from "./common";

export interface Observer {
  next?: (data: Generic) => Promise<void> | void | any;
  error?: (data: Generic) => Promise<void> | void | any;
  complete?: () => Promise<void> | void | any;
  end?: () => Promise<void> | void | any;
}

/**
 * Observable class.
 */
export class Observable {
  /**
   * The array of observers.
   */
  observers: Observer[];

  constructor(observers: Observer[] = []) {
    this.observers = observers;
  }

  /**
   * Call the next function on all observers, if it exists.
   * @param data Data to pass to observers.
   */
  public next(data: Generic) {
    for (const observer of this.observers) {
      observer.next?.(data);
    }
  }

  /**
   * Call the error function on all observers, if it exists.
   * @param data Data to pass to observers.
   */
  public error(data: Generic) {
    for (const observer of this.observers) {
      observer.error?.(data);
    }
  }

  /**
   * Call the complete function on all observers, if it exists.
   */
  public complete() {
    for (const observer of this.observers) {
      observer.complete?.();
    }
  }

  /**
   * Call the end function on all observers, if it exists
   * and remove all observers.
   */
  public end() {
    for (const observer of this.observers) {
      observer.end?.();
    }
    remove(this.observers, () => true);
  }

  /**
   * Add an observer to the array of observers.
   * @param observer The observer to add.
   */
  public observe(observer: Observer) {
    this.observers.push(observer);
  }

  /**
   * Remove an observer from the array of observers.
   * @param observer The observer to remove.
   */
  public unobserve(observer: Observer) {
    remove(this.observers, (o) => o === observer);
    //this.observers = this.observers.filter((o) => o !== observer);
  }
}
