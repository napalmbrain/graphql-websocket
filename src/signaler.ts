import find from "lodash/find";
import remove from "lodash/remove";

import { Resolver } from "./common";

/**
 * Signaler class.
 */
export class Signaler {
  protected callback: (() => void) | void;
  protected handle: Promise<void>;
  protected resolved: boolean;
  protected resolvers: Resolver[];

  // @ts-ignore: `resolve` IS set in the constructor.
  protected resolve: () => void;

  constructor(callback: (() => void) | void) {
    if (callback) {
      this.callback = callback;
    }
    this.resolved = false;
    this.resolvers = [];
    this.handle = new Promise((resolve) => {
      this.resolve = resolve;
    });
  }

  /**
   * Create a promise which is resolved when the
   * signaler is executed.
   * @param args Arguments to resolve with.
   * @returns Promise<any>
   */
  public promise(...args: any[]) {
    const resolver = {} as Resolver;
    resolver.arguments = args;
    resolver.promise = new Promise((resolve) => {
      resolver.resolve = resolve;
    });
    if (this.resolved) {
      resolver.resolve.apply(undefined, resolver.arguments);
    } else {
      this.resolvers.push(resolver);
    }
    return resolver.promise;
  }

  /**
   * Resolve the promise and remove it from the resolvers array.
   * @param promise The promise to remove.
   */
  public unpromise(promise: Promise<unknown> | undefined) {
    const resolver = find(this.resolvers, ({ promise: p }) => promise === p);
    if (resolver) {
      resolver.resolve.apply(undefined, resolver.arguments);
      remove(this.resolvers, (r) => resolver === r);
    }
  }

  /**
   * Resolve pending promises and call the signaler's callback.
   */
  public execute() {
    this.resolved = true;
    for (const resolver of this.resolvers.slice()) {
      // XXX: `unpromise` resolves the promise
      // and removes it from `this.resolvers`.
      this.unpromise(resolver.promise);
    }
    this.callback?.();
    this.resolve();
  }
}
