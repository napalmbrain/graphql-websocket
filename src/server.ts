import { v4 } from "uuid";
import merge from "lodash/merge";
import isFunction from "lodash/isFunction";
import isObject from "lodash/isObject";
import isEmpty from "lodash/isEmpty";
import filter from "lodash/filter";
import reject from "lodash/reject";
import concat from "lodash/concat";
import { WebSocketServer, WebSocket, RawData, ServerOptions } from "ws";
import {
  parse,
  validate,
  execute,
  subscribe,
  DocumentNode,
  ExecutionResult,
  ValidationRule,
  GraphQLSchema,
  GraphQLError,
  OperationDefinitionNode
} from "graphql";
import { makeExecutableSchema } from "@graphql-tools/schema";

import {
  PROTOCOL,
  CODE,
  MESSAGE,
  KEEPALIVE,
  TIMEOUT,
  Message,
  Listeners,
  Serializer,
  Operation,
  Payload,
  Meta,
  randomKeepalive
} from "./common";
import { Timeout } from "./timeout";
import { Signaler } from "./signaler";
import { isAsyncGenerator } from "./utils";
import { Observable, Observer } from "./observable";

/**
 * GraphQL context.
 */
type Context =
  | ((context: Record<string, unknown>) => void)
  | ((context: Record<string, unknown>) => Record<string, unknown>)
  | ((context: Record<string, unknown>) => Promise<void>)
  | ((context: Record<string, unknown>) => Promise<Record<string, unknown>>);

/**
 * Server events.
 */
interface ServerEvents {
  start?: (server: Server) => void | Promise<void>;
  stop?: (server: Server) => void | Promise<void>;
  connection?: (server: Server, cid: string) => void | Promise<void>;
  disconnect?: (server: Server, cid: string) => void | Promise<void>;
  error?: (server: Server, cid: string, error: Error) => void | Promise<void>;
  ping?: (
    server: Server,
    cid: string,
    payload: undefined | Payload
  ) => void | Promise<void>;
  pong?: (
    server: Server,
    cid: string,
    payload: undefined | Payload
  ) => void | Promise<void>;
  keepalive?: (server: Server, cid: string) => void | Promise<void>;
  timeout?: (server: Server, cid: string) => void | Promise<void>;
  close?: (server: Server, cid: string) => void | Promise<void>;
}

/**
 * Server arguments.
 */
interface ServerArguments {
  server?: WebSocketServer | Record<string, any>;
  options?: ServerOptions;
  timeout?: number;
  keepalive?: number;
  operation?: {
    timeout?: number;
    keepalive?: number;
  };
  context?: Context;
  typeDefs?: DocumentNode | string;
  resolvers?: Record<string, any>;
  schema?: GraphQLSchema;
  rules?: Array<ValidationRule>;
  root?: any;
  on?: ServerEvents;
}

/**
 * Connection interface.
 */
interface Connection {
  client: WebSocket;
  listeners: Listeners;
  operations: Map<string, Operation>;
  keepalive: Timeout;
  timeout: Timeout;
  closer: Signaler;
  meta: Meta;
}

/**
 * Server class.
 */
export class Server extends Serializer {
  public sid: string;
  readonly server: WebSocketServer;

  readonly on: ServerEvents;

  protected listeners: Listeners;
  readonly connections: Map<string, Connection>;
  protected operations: Map<string, Operation>;
  protected context?: Context;
  protected schema: GraphQLSchema;

  protected arguments: ServerArguments;

  constructor(args: ServerArguments) {
    super();
    this.arguments = args;
    this.sid = v4();
    this.on = merge({}, args.on);
    if (args.server) {
      this.server = args.server as WebSocketServer;
    } else {
      this.server = new WebSocketServer(args.options);
    }
    if (args.schema) {
      this.schema = args.schema;
    } else if (args.typeDefs && args.resolvers) {
      this.schema = makeExecutableSchema({
        typeDefs: args.typeDefs,
        resolvers: args.resolvers
      });
    } else {
      throw new Error(
        "graphql-websocket: missing schema, typeDefs and/or resolvers."
      );
    }
    this.context = args.context;
    this.listeners = {};
    this.connections = new Map<string, Connection>();
    this.operations = new Map<string, Operation>();
  }

  /**
   * Get and set meta data for a `cid`.
   * @param cid The client id.
   * @param meta The meta data to set.
   * @returns The connection's meta data.
   */
  protected meta(cid: string, meta: Meta) {
    const connection = this.connections.get(cid);
    if (connection) {
      return merge(connection.meta, meta);
    }
  }

  /**
   * Send a message to a client.
   * @param cid The client id.
   * @param message The message to send.
   */
  protected send(cid: string, message: Message) {
    this.connections.get(cid)?.client.send(this.encode(message));
  }

  /**
   * Create an operation.
   * @param cid The client id.
   * @param type The operation type.
   * @param payload The operation payload.
   * @param observers The operation observers.
   * @param timeout The operation timeout.
   * @param send Whether or not to send the operation.
   * @returns The operation.
   */
  protected operation(
    cid: string,
    type: MESSAGE,
    payload: Payload = {},
    observers: Observer[] = [],
    timeout: number = TIMEOUT,
    send: boolean = true
  ): Operation {
    let oid: string;
    do {
      oid = v4();
    } while (this.operations.has(oid));
    const operations = this.operations;
    const operation = {} as Operation;
    operations.set(oid, operation);
    operation.id = oid;
    operation.type = type;
    operation.store = operations;
    operation.arguments = arguments;
    operation.payload = payload;
    operation.observable = new Observable(observers);
    operation.observable.observe({
      end: () => {
        operation.timeout.stop();
        operation.closer.execute();
        operation.store.delete(operation.id);
      }
    });
    operation.closer = new Signaler();
    operation.timeout = new Timeout(() => {
      operation.observable.end();
    }, timeout);
    if (send) {
      if (!isEmpty(payload)) {
        this.send(cid, { type, payload, id: oid });
      } else {
        this.send(cid, { type, id: oid });
      }
    }
    return operation;
  }

  /**
   * The message event listener.
   * @param cid The client id.
   * @param data The data recieved from the client.
   */
  protected async message(cid: string, data: RawData) {
    const message = this.decode(data.toString());
    const mid = message.id;
    const payload = message.payload;
    const oid = payload?.id;
    switch (message.type) {
      case MESSAGE.init:
        if (mid) {
          this.connections.get(cid)?.operations.set(mid, {
            observable: new Observable([
              {
                next: (payload) => {
                  this.send(cid, {
                    type: MESSAGE.ack,
                    id: mid,
                    payload: {
                      cid,
                      sid: this.sid,
                      meta: payload?.meta ? this.meta(cid, payload.meta) : {}
                    }
                  });
                },
                end: () => {
                  this.connections.get(cid)?.operations.delete(mid);
                }
              }
            ])
          } as Operation);
          if (payload) {
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.next(payload);
          } else {
            this.connections.get(cid)?.operations.get(mid)?.observable.next({});
          }
          this.connections.get(cid)?.operations.get(mid)?.observable.complete();
          this.connections.get(cid)?.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.ping:
        if (mid) {
          this.connections.get(cid)?.timeout.refresh();
          if (this.on.ping) {
            await this.on.ping.call(this, this, cid, payload);
          }
          this.connections.get(cid)?.operations.set(mid, {
            observable: new Observable([
              {
                next: (payload) => {
                  this.send(cid, {
                    type: MESSAGE.pong,
                    id: mid,
                    payload: payload ? payload : {}
                  });
                },
                end: () => {
                  this.connections.get(cid)?.operations.delete(mid);
                }
              }
            ])
          } as Operation);
          if (payload) {
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.next(payload);
          } else {
            this.connections.get(cid)?.operations.get(mid)?.observable.next({});
          }
          this.connections.get(cid)?.operations.get(mid)?.observable.complete();
          this.connections.get(cid)?.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.pong:
        if (mid) {
          this.connections.get(cid)?.timeout.refresh();
          if (this.on.pong) {
            await this.on.pong.call(this, this, cid, payload);
          }
          if (payload) {
            this.operations.get(mid)?.observable.next(payload);
          }
          this.operations.get(mid)?.observable.complete();
          this.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.meta:
        if (mid) {
          this.connections.get(cid)?.operations.set(mid, {
            observable: new Observable([
              {
                next: (payload) => {
                  this.send(cid, {
                    payload,
                    type: MESSAGE.meta,
                    id: mid
                  });
                },
                end: () => {
                  this.connections.get(cid)?.operations.delete(mid);
                }
              }
            ])
          } as Operation);
          this.connections
            .get(cid)
            ?.operations.get(mid)
            ?.observable.next({
              meta: payload?.meta
                ? this.meta(cid, payload.meta)
                : this.connections.get(cid)?.meta
            });
          this.connections.get(cid)?.operations.get(mid)?.observable.complete();
          this.connections.get(cid)?.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.subscribe:
        if (mid && payload) {
          this.connections.get(cid)?.operations.set(mid, {
            closer: new Signaler(() => {
              this.connections.get(cid)?.operations.get(mid)?.timeout?.stop();
              this.connections.get(cid)?.operations.get(mid)?.keepalive?.stop();
            }),
            timeout: new Timeout(() => {
              this.connections
                .get(cid)
                ?.operations.get(mid)
                ?.observable.complete();
              this.connections.get(cid)?.operations.get(mid)?.observable.end();
            }, this.arguments.operation?.timeout || this.arguments.timeout || TIMEOUT),
            keepalive: new Timeout(() => {
              this.operation(cid, MESSAGE.operation_ping, { id: mid });
              const operation = this.connections.get(cid)?.operations.get(mid);
              if (operation) {
                operation.keepalive.timeout = randomKeepalive(
                  this.arguments.operation?.keepalive ||
                    this.arguments.keepalive ||
                    KEEPALIVE
                );
                operation.keepalive.refresh();
              }
            }, randomKeepalive(this.arguments.operation?.keepalive || this.arguments.keepalive || KEEPALIVE)),
            observable: new Observable([
              {
                next: (payload) => {
                  this.send(cid, {
                    payload,
                    type: MESSAGE.next,
                    id: mid
                  });
                },
                error: (errors) => {
                  this.send(cid, {
                    type: MESSAGE.error,
                    id: mid,
                    payload: errors
                  });
                },
                complete: () => {
                  this.send(cid, {
                    type: MESSAGE.complete,
                    id: mid
                  });
                },
                end: () => {
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.closer.execute();
                  this.connections.get(cid)?.operations.delete(mid);
                }
              }
            ])
          } as Operation);
          let document: DocumentNode;
          try {
            document = parse(payload.query);
          } catch (error) {
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.error({ errors: [error] });
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.complete();
            this.connections.get(cid)?.operations.get(mid)?.observable.end();
            break;
          }
          const validationErrors = validate(
            this.schema,
            document,
            this.arguments.rules
          );
          if (validationErrors.length) {
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.error({ errors: validationErrors });
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.complete();
            this.connections.get(cid)?.operations.get(mid)?.observable.end();
            break;
          }
          let contextValue: Record<string, unknown> = {};
          const variableValues = payload?.variables;
          const rootValue = this.arguments.root;
          if (this.context) {
            if (isFunction(this.context)) {
              const context = await this.context(contextValue);
              if (context) {
                contextValue = context;
              }
            } else if (isObject(this.context)) {
              contextValue = this.context;
            }
          }
          contextValue.meta = this.connections.get(cid)?.meta;
          const queries = filter(
            document.definitions,
            (definition: OperationDefinitionNode) => {
              return definition.operation === "query";
            }
          );
          const mutations = filter(
            document.definitions,
            (definition: OperationDefinitionNode) => {
              return definition.operation === "mutation";
            }
          );
          const subscriptions = filter(
            document.definitions,
            (definition: OperationDefinitionNode) => {
              return definition.operation === "subscription";
            }
          );
          if ((queries.length || mutations.length) && subscriptions.length) {
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.error({
                errors: [
                  "Cannot query and/or mutate and subscribe at the same time."
                ]
              });
            this.connections
              .get(cid)
              ?.operations.get(mid)
              ?.observable.complete();
            this.connections.get(cid)?.operations.get(mid)?.observable.end();
            break;
          }
          let promises: Array<Promise<any>> = [];
          let response = {};
          let errors: Array<GraphQLError> = [];
          for (const definition of concat(queries, mutations)) {
            const operationName = (definition as OperationDefinitionNode).name
              ?.value;
            const result = execute({
              contextValue,
              document,
              operationName,
              rootValue,
              variableValues,
              schema: this.schema
            });
            const promise = Promise.resolve(result);
            promises.push(promise);
            promise.then((result: ExecutionResult) => {
              if (result.errors?.length) {
                errors = concat(errors, result.errors);
              } else {
                response = merge(response, result);
              }
              promises = reject(promises, (p) => p === promise);
              if (promises.length === 0) {
                if (!isEmpty(response)) {
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.observable.next(response);
                }
                if (errors.length) {
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.observable.error({
                      errors
                    });
                }
                this.connections
                  .get(cid)
                  ?.operations.get(mid)
                  ?.observable.complete();
                this.connections
                  .get(cid)
                  ?.operations.get(mid)
                  ?.observable.end();
              }
            });
          }
          for (const definition of subscriptions) {
            const operationName = (definition as OperationDefinitionNode).name
              ?.value;
            const promise = subscribe({
              contextValue,
              document,
              operationName,
              rootValue,
              variableValues,
              schema: this.schema
            });
            promises.push(promise);
            promise.then(async (result) => {
              if (isAsyncGenerator(result)) {
                // XXX: the operation is already complete.
                if (!this.connections.get(cid)?.operations.get(mid)) {
                  return;
                }
                let subscription: any;
                const complete = this.connections
                  .get(cid)
                  ?.operations.get(mid)
                  ?.closer.promise({ done: true, closed: true });
                const closed = this.connections
                  .get(cid)
                  ?.closer.promise({ done: true, closed: true });
                do {
                  subscription = await Promise.race([
                    complete,
                    closed,
                    (result as AsyncGenerator).next()
                  ]);
                  if (!subscription.done) {
                    this.connections
                      .get(cid)
                      ?.operations.get(mid)
                      ?.observable.next(subscription.value);
                  } else {
                    promises = reject(promises, (p) => p === promise);
                    if (promises.length === 0) {
                      this.connections
                        .get(cid)
                        ?.operations.get(mid)
                        ?.observable.complete();
                      this.connections
                        .get(cid)
                        ?.operations.get(mid)
                        ?.observable.end();
                    }
                  }
                } while (!subscription.done);
                if (subscription.closed) {
                  await (result as AsyncGenerator).return(null);
                }
                this.connections
                  .get(cid)
                  ?.operations.get(mid)
                  ?.closer.unpromise(complete);
                this.connections.get(cid)?.closer.unpromise(closed);
              } else {
                if ((result as ExecutionResult).errors?.length) {
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.observable.error(result);
                } else {
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.observable.next(result);
                }
                promises = reject(promises, (p) => p === promise);
                if (promises.length === 0) {
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.observable.complete();
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.observable.end();
                }
              }
            });
          }
        }
        break;
      case MESSAGE.operation_ping:
        if (mid && oid && this.connections.get(mid)?.operations.get(oid)) {
          this.connections.get(cid)?.operations.get(oid)?.timeout?.refresh();
          this.send(cid, {
            type: MESSAGE.operation_pong,
            id: mid,
            payload: { id: oid }
          });
        }
        break;
      case MESSAGE.operation_pong:
        if (mid && oid) {
          this.connections.get(cid)?.operations.get(oid)?.timeout?.refresh();
          this.operations.get(mid)?.observable.complete();
          this.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.complete:
        if (mid) {
          this.connections.get(cid)?.operations.get(mid)?.observable.complete();
          this.connections.get(cid)?.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.close:
        if (mid && oid) {
          this.connections.get(cid)?.operations.set(mid, {
            closer: new Signaler(),
            observable: new Observable([
              {
                complete: () => {
                  this.send(cid, {
                    type: MESSAGE.complete,
                    id: mid
                  });
                },
                end: () => {
                  this.connections
                    .get(cid)
                    ?.operations.get(mid)
                    ?.closer.execute();
                  this.connections.get(cid)?.operations.delete(mid);
                }
              }
            ])
          } as Operation);
          this.connections
            .get(cid)
            ?.operations.get(oid)
            ?.observable.observe({
              complete: () => {
                this.connections
                  .get(cid)
                  ?.operations.get(mid)
                  ?.observable.complete();
              },
              end: () => {
                this.connections
                  .get(cid)
                  ?.operations.get(mid)
                  ?.observable.end();
              }
            });
          this.connections.get(cid)?.operations.get(oid)?.observable.complete();
          this.connections.get(cid)?.operations.get(oid)?.observable.end();
        }
    }
  }

  /**
   * The error event listener.
   * @param cid The client id.
   * @param error The error.
   */
  protected async error(cid: string, error: Error) {
    if (this.on.error) {
      await this.on.error.call(this, this, cid, error);
    } else {
      console.error(cid, error);
    }
  }

  /**
   * The close event listener.
   * @param cid The client id.
   * @param socket The socket.
   * @param code The code.
   * @param reason The reason.
   */
  protected async close(
    cid: string,
    socket: WebSocket,
    code: number,
    reason: Buffer
  ) {
    if (this.on.close) {
      await this.on.close.call(this, this, cid);
    }
    this.connections.get(cid)?.closer.execute();
  }

  /**
   * Attach listeners.
   * @param cid The client id.
   */
  protected listen(cid: string) {
    const message = this.message.bind(this, cid);
    const error = this.error.bind(this, cid);
    const close = this.close.bind(this, cid);
    const connection = this.connections.get(cid);
    if (connection) {
      connection.listeners.message = message;
      connection.listeners.error = error;
      connection.listeners.close = close;
    }
    this.connections.get(cid)?.client.on("message", message);
    this.connections.get(cid)?.client.on("error", error);
    this.connections.get(cid)?.client.on("close", close);
  }

  /**
   * Detach listeners.
   * @param cid The client id.
   */
  protected unlisten(cid: string) {
    if (this.connections.get(cid)) {
      const listeners = this.connections.get(cid)?.listeners;
      for (const listener in listeners) {
        this.connections.get(cid)?.client.off(listener, listeners[listener]);
        delete listeners[listener];
      }
    }
  }

  /**
   * Add a client.
   * @param cid The client id.
   * @param client The WebSocket client.
   */
  protected add(cid: string, client: WebSocket) {
    let keepalive = KEEPALIVE;
    if (this.arguments.keepalive && this.arguments.keepalive > 0) {
      keepalive = this.arguments.keepalive;
    }
    let timeout = TIMEOUT;
    if (this.arguments.timeout && this.arguments.timeout > 0) {
      timeout = this.arguments.timeout;
    }
    this.connections.set(cid, {
      client,
      listeners: {},
      meta: {},
      operations: new Map<string, Operation>(),
      keepalive: new Timeout(async () => {
        this.ping(cid);
        if (this.on.keepalive) {
          await this.on.keepalive.call(this, this, cid);
        }
        const connection = this.connections.get(cid);
        if (connection) {
          connection.keepalive.timeout = randomKeepalive(keepalive);
          connection.keepalive.refresh();
        }
      }, randomKeepalive(keepalive)),
      timeout: new Timeout(async () => {
        if (this.on.timeout) {
          await this.on.timeout.call(this, this, cid);
        } else {
          console.error(`graphql-websocket: client ${cid} timed out.`);
        }
        this.connections.get(cid)?.closer.execute();
      }, timeout),
      closer: new Signaler(() => {
        this.disconnect(cid);
      })
    });
    this.listen(cid);
  }

  /**
   * Remove a client.
   * @param cid The client id.
   */
  protected remove(cid: string) {
    this.unlisten(cid);
    const connection = this.connections.get(cid);
    if (connection) {
      for (const oid of connection.operations.keys()) {
        connection.operations.get(oid)?.observable.complete();
        connection.operations.get(oid)?.observable.end();
      }
      this.connections.delete(cid);
    }
  }

  /**
   * The connection event listener.
   * @param client
   * @returns
   */
  protected async connection(client: WebSocket) {
    if (!(client.protocol === PROTOCOL)) {
      console.error("graphql-websocket: invalid protocol.");
      client.close(CODE.protocol, "graphql-websocket: invalid protocol.");
      return;
    }
    let cid: string;
    do {
      cid = v4();
    } while (this.connections.has(cid));
    this.add(cid, client);
    if (this.on.connection) {
      await this.on.connection.call(this, this, cid);
    }
  }

  /**
   * The disconnect event listener.
   * @param cid The client id.
   */
  protected async disconnect(cid: string) {
    if (this.on.disconnect) {
      await this.on.disconnect.call(this, this, cid);
    }
    this.connections.get(cid)?.timeout.stop();
    this.connections.get(cid)?.client.close();
    this.remove(cid);
  }

  /**
   * Begin listening for connections.
   */
  public async start() {
    const connection = this.connection.bind(this);
    this.listeners.connect = connection;
    this.server.on("connection", connection);
    if (this.on.start) {
      await this.on.start.call(this, this);
    }
  }

  /**
   * Stop listening for connections and clean up.
   */
  public async stop() {
    if (this.on.stop) {
      await this.on.stop.call(this, this);
    }
    for (const listener in this.listeners) {
      this.server.off(listener, this.listeners[listener]);
      delete this.listeners[listener];
    }
    for (const cid of this.connections.keys()) {
      this.connections.get(cid)?.closer.execute();
    }
  }

  /**
   * Ping a client.
   * @param cid The client id.
   * @param payload The payload.
   * @param observer The observer.
   * @param timeout The timeout.
   * @returns The ping payload or { closed: true }.
   */
  public async ping(
    cid: string,
    payload: Payload = {},
    observer: Observer = {},
    timeout: number = TIMEOUT
  ): Promise<Payload | void> {
    const observers: Observer[] = [];
    if (!isEmpty(observer)) {
      observers.push(observer);
    }
    const promise = new Promise((resolve, reject) => {
      let response: Payload;
      observers.push({
        next: (p: Payload) => (response = p),
        complete: () => resolve(response),
        error: reject
      });
    });
    const { closer } = this.operation(
      cid,
      MESSAGE.ping,
      payload,
      observers,
      timeout
    );
    const closed = closer.promise({ closed: true });
    return Promise.race([promise, closed]);
  }
}
