import { v4 } from "uuid";
import isEmpty from "lodash/isEmpty";
import isFunction from "lodash/isFunction";
import filter from "lodash/filter";
import { DocumentNode, OperationDefinitionNode, print, parse } from "graphql";

import {
  PROTOCOL,
  MESSAGE,
  TIMEOUT,
  Message,
  Listeners,
  Serializer,
  Operation,
  Payload,
  Meta,
  MetaFunction,
  Subscription
} from "./common";
import { Timeout } from "./timeout";
import { Signaler } from "./signaler";
import { Observable, Observer } from "./observable";
import { isBrowser, sleep } from "./utils";

/**
 * Client events.
 */
interface ClientEvents {
  connect?: (
    client: Client,
    { connected }: { connected: boolean }
  ) => void | Promise<void>;
  reconnect?: (client: Client) => void | Promise<void>;
  disconnect?: (client: Client) => void | Promise<void>;
  freeze?: (client: Client) => void | Promise<void>;
  unfreeze?: (client: Client) => void | Promise<void>;
  error?: (client: Client, error: Event) => void | Promise<void>;
  ping?: (client: Client, payload: undefined | Payload) => void | Promise<void>;
  pong?: (client: Client, payload: undefined | Payload) => void | Promise<void>;
  timeout?: (client: Client) => void | Promise<void>;
  close?: (client: Client) => void | Promise<void>;
}

/**
 * Client arguments.
 */
export interface ClientArguments {
  uri: string;
  implementation?: any;
  meta?: Meta | MetaFunction;
  timeout?: number;
  on?: ClientEvents;
}

/**
 * GraphQL WebSocket client.
 */
export class Client extends Serializer {
  public sid?: string;
  public cid?: string;

  public connected: Promise<{ connected: boolean }>;

  readonly on: ClientEvents;

  protected arguments: ClientArguments;
  protected listeners: Listeners;
  protected operations: Map<string, Operation>;
  protected frozen: Map<string, Operation>;
  // @ts-ignore: `socket` IS set via `connect`.
  protected socket: WebSocket;

  protected listening: boolean;
  protected timeout?: Timeout;

  protected state: "frozen" | "unfrozen";

  constructor(args: ClientArguments) {
    super();
    this.arguments = args;
    this.on = args.on || {};
    this.listeners = {};
    this.operations = new Map<string, Operation>();
    this.frozen = new Map<string, Operation>();
    this.state = "frozen";
    this.listening = false;
    this.connected = this.connect().then(({ connected }) => {
      if (connected) {
        this.unfreeze();
      }
      return { connected };
    });
  }

  /**
   * Send a message to the server.
   * @param message The message to send.
   */
  protected send(message: Message) {
    this.connected.then(async ({ connected }) => {
      if (connected) {
        while (this.socket.readyState === this.socket.CONNECTING) {
          await sleep(50);
        }
        this.socket.send(this.encode(message));
      } else {
        console.error("graphql-websocket: trying to send while not connected.");
      }
    });
  }

  /**
   * Create an operation.
   * @param type The operation type.
   * @param payload The operation payload.
   * @param observers The operation observers.
   * @param timeout The operation timeout.
   * @param send Whether or not to send the operation.
   * @returns The operation.
   */
  protected operation(
    type: MESSAGE,
    payload: Payload = {},
    observers: Observer[] = [],
    timeout: number = TIMEOUT,
    send: boolean = true
  ): Operation {
    let oid: string;
    do {
      oid = v4();
    } while (this.operations.has(oid) || this.frozen.has(oid));
    const operations =
      type === MESSAGE.init || this.state === "unfrozen"
        ? this.operations
        : this.frozen;
    const operation = {} as Operation;
    operations.set(oid, operation);
    operation.id = oid;
    operation.type = type;
    operation.store = operations;
    operation.arguments = arguments;
    operation.payload = payload;
    operation.observable = new Observable(observers);
    operation.observable.observe({
      end: () => {
        operation.timeout?.stop();
        operation.closer.execute();
        operation.store.delete(operation.id);
      }
    });
    operation.closer = new Signaler();
    operation.timeout = new Timeout(() => {
      operation.observable.end();
    }, this.arguments.timeout || timeout);
    if (!(type === MESSAGE.init) && this.state === "frozen") {
      operation.timeout.stop();
    }
    if (send && this.state === "unfrozen") {
      if (!isEmpty(payload)) {
        this.send({ type, payload, id: oid });
      } else {
        this.send({ type, id: oid });
      }
    }
    return operation;
  }

  protected restart(operation: Operation) {
    if (this.state === "unfrozen") {
      if (!isEmpty(operation.payload)) {
        this.send({
          type: operation.type,
          payload: operation.payload,
          id: operation.id
        });
      } else {
        this.send({ type: operation.type, id: operation.id });
      }
    }
  }

  /**
   * Begin listening for messages.
   */
  protected listen() {
    if (this.listening) {
      throw new Error(
        "graphql-websocket: already listening, call unlisten first."
      );
    }
    this.listening = true;
    const message = this.message.bind(this);
    const error = this.error.bind(this);
    const close = this.close.bind(this);
    this.listeners.message = message;
    this.listeners.error = error;
    this.listeners.close = close;
    this.socket.addEventListener("message", message);
    this.socket.addEventListener("error", error);
    this.socket.addEventListener("close", close);
  }

  /**
   * Stop listening for messages.
   */
  protected unlisten() {
    for (const listener in this.listeners) {
      this.socket.removeEventListener(listener, this.listeners[listener]);
      delete this.listeners[listener];
    }
    this.listening = false;
  }

  /**
   * The message event listener.
   * @param data The data recieved from the server.
   */
  protected async message(data: any) {
    const message = this.decode(data.data);
    const mid = message.id;
    const payload = message.payload;
    const oid = payload?.id;
    switch (message.type) {
      case MESSAGE.ack:
        if (mid && payload && payload.sid && payload.cid) {
          this.operations.get(mid)?.observable.next(payload);
          this.operations.get(mid)?.observable.complete();
        } else {
          this.operations.get(mid)?.observable.error({
            errors: ["Missing message id or payload."]
          });
        }
        this.operations.get(mid)?.observable.end();
        break;
      case MESSAGE.ping:
        if (mid) {
          this.timeout?.refresh();
          if (this.on.ping) {
            await this.on.ping.call(this, this, payload);
          }
          if (payload) {
            this.send({
              type: MESSAGE.pong,
              id: mid,
              payload
            });
          } else {
            this.send({
              type: MESSAGE.pong,
              id: mid
            });
          }
        }
        break;
      case MESSAGE.pong:
        if (mid) {
          this.timeout?.refresh();
          if (this.on.pong) {
            await this.on.pong.call(this, this, payload);
          }
          if (payload) {
            this.operations.get(mid)?.observable.next(payload);
          }
          this.operations.get(mid)?.observable.complete();
          this.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.operation_ping:
        if (mid && oid && this.operations.get(oid)) {
          this.operations.get(oid)?.timeout.refresh();
          this.send({
            type: MESSAGE.operation_pong,
            id: mid,
            payload: { id: oid }
          });
        }
        break;
      case MESSAGE.operation_pong:
        if (mid && oid) {
          this.operations.get(oid)?.timeout?.refresh();
          this.operations.get(mid)?.observable.complete();
          this.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.meta:
        if (mid && payload && payload.meta) {
          this.operations.get(mid)?.observable.next(payload);
          this.operations.get(mid)?.observable.complete();
          this.operations.get(mid)?.observable.end();
        }
        break;
      case MESSAGE.next:
        if (mid && payload) {
          this.operations.get(mid)?.observable.next(payload);
        }
        break;
      case MESSAGE.error:
        if (mid && payload) {
          this.operations.get(mid)?.observable.error(payload);
        }
        break;
      case MESSAGE.complete:
        if (mid) {
          this.operations.get(mid)?.observable.complete();
          this.operations.get(mid)?.observable.end();
        }
        break;
    }
  }

  /**
   * The error event listener.
   * @param error The error.
   */
  protected async error(error: Event) {
    if (this.on.error) {
      await this.on.error.call(this, this, error);
    } else {
      console.error("graphql-websocket: ", error);
    }
  }

  /**
   * The close event listener.
   */
  protected async close() {
    this.unlisten();
    this.timeout?.stop();
    if (this.on.close) {
      await this.on.close.call(this, this);
    } else {
      console.log(
        `graphql-websocket: connection closed by server ${this.sid}.`
      );
      await this.reconnect();
    }
  }

  /**
   * Initiate a connection to the server.
   * @param timeout The connection attempt timeout.
   * @returns Promise<{ connected: boolean }>
   */
  public async connect(timeout: number = 0) {
    if (isBrowser()) {
      this.socket = new window.WebSocket(this.arguments.uri, PROTOCOL);
    } else if (this.arguments.implementation) {
      this.socket = new this.arguments.implementation(
        this.arguments.uri,
        PROTOCOL
      );
    } else {
      throw new Error(
        "graphql-websocket: no websocket implementation provided."
      );
    }
    let meta: Meta | undefined;
    if (isFunction(this.arguments.meta)) {
      meta = await this.arguments.meta();
    } else {
      meta = this.arguments.meta;
    }
    let operation: Operation;
    const promise = new Promise(
      (
        resolve: (payload: Payload) => void,
        reject: (error: Payload) => void
      ) => {
        let open: any;
        let error: any;
        let result: any;
        operation = this.operation(
          MESSAGE.init,
          meta ? { meta } : {},
          [
            {
              next: (r) => (result = r),
              complete: () => resolve(result),
              error: (error) => reject(error),
              end: () => {
                this.socket.removeEventListener("open", open);
                this.socket.removeEventListener("error", error);
              }
            }
          ],
          timeout > 0 ? timeout : TIMEOUT,
          false // XXX: don't send the operation automatically.
        );
        open = async () => {
          while (this.socket.readyState === this.socket.CONNECTING) {
            await sleep(50);
          }
          this.listen();
          this.socket.send(
            this.encode({
              type: operation.type,
              id: operation.id,
              payload: operation.payload
            })
          );
        };
        error = (event: Event) => {
          operation.observable.error(event);
          operation.observable.end();
        };
        this.socket.addEventListener("open", open);
        this.socket.addEventListener("error", error);
      }
    )
      .then((payload) => {
        this.sid = payload?.sid;
        this.cid = payload?.cid;
        let timeout = TIMEOUT;
        if (this.arguments.timeout && this.arguments.timeout > 0) {
          timeout = this.arguments.timeout;
        }
        this.timeout = new Timeout(async () => {
          if (this.on.timeout) {
            await this.on.timeout.call(this, this);
          } else {
            console.error(`graphql-websocket: server ${this.sid} timed out.`);
            await this.reconnect();
          }
        }, timeout);
        return { connected: true };
      })
      .catch(async (error) => {
        if (this.on.error) {
          await this.on.error.call(this, this, error);
        } else {
          console.error("graphql-websocket: connect error.", error);
        }
        return { connected: false };
      });
    // @ts-ignore: `operation` IS assigned.
    const expired = operation.timeout.promise({ connected: false });
    return (this.connected = Promise.race([promise, expired]).then(
      async ({ connected }) => {
        if (this.on.connect) {
          await this.on.connect.call(this, this, { connected });
        }
        return { connected };
      }
    ));
  }

  /**
   * Reconnect to the server.
   * @param interval Attempt interval.
   */
  public async reconnect(interval: number = 1000) {
    this.freeze();
    if (this.on.reconnect) {
      await this.on.reconnect.call(this, this);
    }
    let connection: { connected: boolean };
    do {
      console.log("graphql-websocket: reconnecting.");
      connection = await this.connect();
      await sleep(interval);
    } while (!connection.connected);
    this.unfreeze();
  }

  /**
   * Disconnect from the server and end all pending operations.
   */
  public async disconnect() {
    this.unlisten();
    this.timeout?.stop();
    if (this.on.disconnect) {
      await this.on.disconnect.call(this, this);
    }
    for (const operation of this.operations.values()) {
      operation.observable.complete();
      operation.observable.end();
    }
    while (this.socket.readyState === this.socket.CONNECTING) {
      await sleep(50);
    }
    this.socket.close();
    this.connected = Promise.resolve({ connected: false });
  }

  /**
   * Freeze all current operations.
   * All new operations will be created in the frozen state.
   */
  public async freeze() {
    if (this.on.freeze) {
      await this.on.freeze.call(this, this);
    }
    this.state = "frozen";
    for (const [oid, operation] of this.operations) {
      if (operation.type === MESSAGE.init) {
        continue;
      }
      operation.timeout?.stop();
      this.frozen.set(oid, operation);
      operation.store = this.frozen;
      this.operations.delete(oid);
    }
  }

  /**
   * Unfreeze all frozen operations.
   * All new operations will be created in the active state.
   */
  public async unfreeze() {
    if (this.on.unfreeze) {
      await this.on.unfreeze.call(this, this);
    }
    this.state = "unfrozen";
    for (const [oid, operation] of this.frozen) {
      this.frozen.delete(oid);
      //this.operation.apply(this, operation.arguments);
      this.operations.set(oid, operation);
      this.restart(operation);
    }
  }

  /**
   * Get and set metadata on the server for the client.
   * @param meta The data to set on the server for the client.
   * @param observer The observer.
   * @param timeout The timeout for the operation.
   * @returns Metadata for the client on the server.
   */
  public async meta(
    meta: Meta | MetaFunction = {},
    observer: Observer = {},
    timeout: number = TIMEOUT
  ): Promise<Payload> {
    const observers: Observer[] = [];
    if (!isEmpty(observer)) {
      observers.push(observer);
    }
    const promise = new Promise((resolve, reject) => {
      let response: Payload;
      observers.push({
        next: (p: Payload) => (response = p),
        complete: () => resolve(response),
        error: reject
      });
    });
    if (isFunction(meta)) {
      meta = await meta();
    }
    const { closer } = this.operation(
      MESSAGE.meta,
      meta ? { meta } : {},
      observers,
      timeout
    );
    const closed = closer.promise({ closed: true });
    return Promise.race([promise, closed]);
  }

  /**
   * Ping the server.
   * @param payload The payload.
   * @param observer The observer.
   * @param timeout The timeout.
   * @returns The ping payload or { closed: true }.
   */
  public async ping(
    payload: Payload = {},
    observer: Observer = {},
    timeout: number = TIMEOUT
  ): Promise<Payload | void> {
    const observers: Observer[] = [];
    if (!isEmpty(observer)) {
      observers.push(observer);
    }
    const promise = new Promise((resolve, reject) => {
      let response: Payload;
      observers.push({
        next: (p: Payload) => (response = p),
        complete: () => resolve(response),
        error: reject
      });
    });
    const { closer } = this.operation(
      MESSAGE.ping,
      payload,
      observers,
      timeout
    );
    const closed = closer.promise({ closed: true });
    return Promise.race([promise, closed]);
  }

  /**
   * Query, mutate and/or subscribe.
   * @param subscription The subscription.
   * @param observer The observer.
   * @returns An operation.
   */
  public subscibe(subscription: Subscription, observer: Observer = {}) {
    const observers: Observer[] = [];
    if (!isEmpty(observer)) {
      observers.push(observer);
    }
    let document: DocumentNode;
    if (typeof subscription.query === "string") {
      document = parse(subscription.query);
    } else {
      document = subscription.query;
    }
    const queries = filter(
      document.definitions,
      (definition: OperationDefinitionNode) => {
        return definition.operation === "query";
      }
    );
    const mutations = filter(
      document.definitions,
      (definition: OperationDefinitionNode) => {
        return definition.operation === "mutation";
      }
    );
    const subscriptions = filter(
      document.definitions,
      (definition: OperationDefinitionNode) => {
        return definition.operation === "subscription";
      }
    );
    if ((queries.length || mutations.length) && subscriptions.length) {
      throw new Error(
        "Cannot query and/or mutate and subscribe at the same time."
      );
    }
    if (typeof subscription.query !== "string") {
      subscription.query = print(subscription.query);
    }
    return this.operation(MESSAGE.subscribe, subscription, observers, TIMEOUT);
  }

  public complete(operation: Operation, observer: Observer = {}): Operation {
    const observers: Observer[] = [];
    if (!isEmpty(observer)) {
      observers.push(observer);
    }
    return this.operation(MESSAGE.close, { id: operation.id }, observers);
  }
}
