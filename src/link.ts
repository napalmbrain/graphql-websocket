import {
  ApolloLink,
  Operation,
  FetchResult,
  Observable
} from "@apollo/client/core";
import isPlainObject from "lodash/isPlainObject";

import { Meta, MetaFunction, Payload } from "./common";
import { Client, ClientArguments } from "./client";
import { Observer } from "./observable";

/**
 * GraphQL WebSocket link for Apollo.
 */
export class Link extends ApolloLink {
  protected client;

  constructor(client: Client | ClientArguments) {
    super();
    if (isPlainObject(client)) {
      this.client = new Client(client as ClientArguments);
    } else {
      this.client = client as Client;
    }
  }

  /**
   * Subscribe to a request.
   * @param operation
   * @returns
   */
  public request(operation: Operation): Observable<FetchResult> {
    return new Observable((observer) => {
      const { observable } = this.client.subscibe(operation, observer);
      return () => observable.end();
    });
  }

  /**
   * Connect to a server.
   */
  public async connect() {
    await this.client.connect();
  }

  /**
   * Reconnect to a server.
   */
  public async reconnect() {
    await this.client.reconnect();
  }

  /**
   * Disconnect from a server.
   */
  public async disconnect() {
    await this.client.disconnect();
  }

  /**
   * Freeze the client.
   */
  public freeze() {
    this.client.freeze();
  }

  /**
   * Unfreeze the client.
   */
  public unfreeze() {
    this.client.unfreeze();
  }

  /**
   * Get and set meta data on the server.
   * @param meta The meta data to set.
   * @param observer The observer.
   * @returns Meta data from the server.
   */
  public meta(meta: Meta | MetaFunction = {}, observer: Observer = {}) {
    return this.client.meta(meta, observer);
  }

  /**
   * Ping the server.
   * @param payload The ping payload.
   * @param observer The observer.
   * @returns The ping payload.
   */
  public ping(payload: Payload, observer: Observer = {}) {
    return this.client.ping(payload, observer);
  }
}
