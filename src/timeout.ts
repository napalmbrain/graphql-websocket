import find from "lodash/find";
import remove from "lodash/remove";

import { Resolver } from "./common";
import { isBrowser } from "./utils";

/**
 * Timeout class.
 */
export class Timeout {
  protected callback: () => void;
  protected handle?: any;
  protected resolved: boolean;
  protected resolvers: Resolver[];

  public timeout: number;

  constructor(callback: () => void, timeout: number) {
    this.callback = callback;
    this.timeout = timeout;
    this.resolved = false;
    this.resolvers = [];
    this.start();
  }

  /**
   * Resolve pending promises and call the timeout's callback.
   */
  protected resolve() {
    this.resolved = true;
    for (const resolver of this.resolvers.slice()) {
      // XXX: `unpromise` resolves the promise
      // and removes it from `this.resolvers`.
      this.unpromise(resolver.promise);
    }
    this.callback();
  }

  /**
   * Create a promise which is resolved when the
   * timeout is executed.
   * @param args Arguments to resolve with.
   * @returns Promise<any>
   */
  public promise(...args: any[]) {
    const resolver = {} as Resolver;
    resolver.arguments = args;
    resolver.promise = new Promise((resolve) => {
      resolver.resolve = resolve;
    });
    if (this.resolved) {
      resolver.resolve.apply(undefined, resolver.arguments);
    } else {
      this.resolvers.push(resolver);
    }
    return resolver.promise;
  }

  /**
   * Resolve the promise and remove it from the resolvers array.
   * @param promise The promise to remove.
   */
  public unpromise(promise: Promise<unknown>) {
    const resolver = find(this.resolvers, ({ promise: p }) => promise === p);
    if (resolver) {
      resolver.resolve.apply(undefined, resolver.arguments);
      remove(this.resolvers, (r) => resolver === r);
    }
  }

  /**
   * Refresh the timeout.
   */
  public refresh() {
    this.stop();
    this.start();
  }

  /**
   * Start the timeout.
   */
  public start() {
    this.handle = setTimeout(this.resolve.bind(this), this.timeout);
    if (!isBrowser()) {
      this.handle.unref();
    }
  }

  /**
   * Stop the timeout.
   */
  public stop() {
    clearTimeout(this.handle);
  }
}
