import has from "lodash/has";

/**
 * Asynchronously sleep for the specified timeout.
 * @param timeout The timeout.
 * @returns Promise<unknown>
 */
export function sleep(timeout: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
}

/**
 * Determines if the environment is the browser.
 * @returns true if the environment is the browser, else false.
 */
export function isBrowser() {
  if (typeof window === "object") {
    return true;
  }
  return false;
}

/**
 * Determines if `object` is an AsyncGenerator.
 * @param object The object to check.
 * @returns true if `object` is an AsyncGenerator, else false.
 */
export function isAsyncGenerator(object: any) {
  if (has(object, Symbol.asyncIterator)) {
    return true;
  }
  return false;
}
