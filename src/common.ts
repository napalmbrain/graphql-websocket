import { Signaler } from "./signaler";
import { Timeout } from "./timeout";
import { Observable } from "./observable";
import { DocumentNode } from "graphql";

export const TIMEOUT = 10_000;
export const KEEPALIVE = TIMEOUT / 2;

export const PROTOCOL = "graphql-websocket";

export enum MESSAGE {
  init = "graphql-websocket-init",
  ack = "graphql-websocket-ack",
  ping = "graphql-websocket-ping",
  pong = "graphql-websocket-pong",
  operation_ping = "graphql-websocket-operation-ping",
  operation_pong = "graphql-websocket-operation-pong",
  subscribe = "graphql-websocket-subscribe",
  next = "graphql-websocket-next",
  error = "graphql-websocket-error",
  complete = "graphql-websocket-complete",
  close = "graphql-websocket-close",
  meta = "graphql-websocket-meta"
}

export enum CODE {
  protocol = 4006
}

export interface Generic {
  [key: string | number]: any;
}

export interface Listeners extends Generic {}
export interface Meta extends Generic {}
export interface Payload extends Generic {}

export type MetaFunction = (() => Meta) | (() => Promise<Meta>);

export interface Message {
  type: MESSAGE;
  id: string;
  payload?: Payload;
}

export interface Subscription {
  [key: string]: any;
  query: string | DocumentNode;
  variables?: Record<string, any>;
}

export interface Operation {
  id: string;
  type: MESSAGE;
  arguments: any;
  payload: Payload;
  store: Map<string, Operation>;
  observable: Observable;
  closer: Signaler;
  timeout: Timeout;
  keepalive: Timeout;
}

export interface Operations {
  [key: string]: Operation;
}

export interface Resolver {
  resolve: ((value: any) => void) | (() => void);
  promise: Promise<any>;
  arguments: any;
}

export class Serializer {
  protected encode(message: { [key: string | number]: any }): string {
    return JSON.stringify(message);
  }
  protected decode(message: string): Message {
    return JSON.parse(message);
  }
}

export function randomKeepalive(keepalive: number) {
  return (
    keepalive - keepalive / 2 + Math.floor((keepalive / 2) * Math.random())
  );
}
